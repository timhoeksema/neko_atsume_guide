class Cat {
  const Cat({this.name, this.personality, this.fur, this.power, this.goodies, this.imageUrl});

  final String name;
  final String personality;
  final String fur;
  final int power;
  final String goodies;
  final String imageUrl;
}

final List<Cat> cat = <Cat>[
  Cat(
    name: 'Fred',
    personality: 'Lady-killer',
    fur: 'Orange Tabby',
    power: 150,
    goodies: 'Royal Bed, Tower of Treats, Soccer Ball',
    imageUrl: 'https://vignette.wikia.nocookie.net/nekoatsume/images/7/7b/Album_Fred.png/revision/latest?cb=20151113201217',
  ),
  Cat(
    name: 'Gozer',
    personality: 'Sore Loser',
    fur: 'Tortoiseshell Tabby',
    power: 155,
    goodies: 'Fairy-tale Parasol, Zanzibar Cushion, Soccer Ball',
    imageUrl: 'https://vignette.wikia.nocookie.net/nekoatsume/images/6/64/Album_Gozer.png/revision/latest?cb=20151113205722',
  ),
];